package com;

import com.alibaba.fastjson.JSONObject;
import com.fz.User;
import com.fz.util.DbUtil;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Created by webrx on 2017-08-16.
 */
public class Demo {

    @Test
    public void uu(){
        int i = 30;
        User u = new User();
        u.aaa();
        //u.setId(100);u.setName("李四四");
        //System.out.println(u);
    }

    @Test
    public void dd(){
        DbUtil du = new DbUtil();
        List<Map<String,Object>> list = du.query("oa_admin");
        System.out.println(list.size());
        for (Map<String,Object> mm :list){
            System.out.println(mm.get("truename"));
        }
    }


    @Test
    public void ff(){
        String[] addr = new String[]{"北京市","郑州市","上海市"};
        System.out.println(JSONObject.toJSONString(addr));
    }


    @Test
    public void oo(){
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            String url = "jdbc:oracle:thin:@localhost:1521:ORCL";
            String uid ="system";
            String pwd = "fengze";
            Connection conn = DriverManager.getConnection(url,uid,pwd);
            System.out.println(conn);

            String sql = "select * from book";
            PreparedStatement pst = conn.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            while(rs.next()){
                System.out.println(rs.getString("name"));
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void aa() {
        Properties pro = new Properties();
        try {
            //加载配置文件
            pro.load(Demo.class.getClassLoader().getResourceAsStream("db.properties"));
            System.out.println(pro.get("db.driver"));
            System.out.println(pro.get("db.url"));
            System.out.println(pro.get("db.user"));
            System.out.println(pro.get("db.password"));

            Class.forName(pro.getProperty("db.driver"));
            Connection conn = DriverManager.getConnection(pro.getProperty("db.url"), pro.getProperty("db.user"), pro.getProperty("db.password"));
            System.out.println(conn);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
