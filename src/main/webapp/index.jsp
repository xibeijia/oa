<%@ page import="com.fz.util.DbUtil" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <title>maven java web</title>
    <link rel="stylesheet" href="css/index.min.css">
</head>
<body>
<%
    DbUtil du = new DbUtil();
    pageContext.setAttribute("admin",du.query("oa_admin"));
%>
<hr>
<c:forEach items="${admin}" var="a">
    ${a.truename}<br>
</c:forEach>

<hr>

<a href="show">show</a><br><br>
<a href="abc">abc</a>
<br><br>
<hr>
<h2>Hello World!</h2>
<%
    out.print(2*2);
%>
<hr>
<c:forEach begin="1" end="10" var="i">
    <h3>${i}</h3>
</c:forEach>
<hr>

${2*9}
</body>
</html>
