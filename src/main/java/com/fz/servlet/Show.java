package com.fz.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by webrx on 2017-08-16.
 */
//@WebServlet("/show")
    @WebServlet(urlPatterns = {"/news","/admin/news","/book/info.action"})
public class Show extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset-utf-8");
        PrintWriter out = resp.getWriter();
        out.print("maven servlet 中文");

        out.flush();
        out.close();
    }
}
