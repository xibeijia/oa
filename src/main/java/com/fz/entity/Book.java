package com.fz.entity;

import lombok.*;

/**
 * Created by webrx on 2017-08-16.
 */
@Data @AllArgsConstructor @NoArgsConstructor
//@Setter
public class Book {
    private int id;
    //@Getter @Setter
    private String name;
    private double price;
    private String content;
}
